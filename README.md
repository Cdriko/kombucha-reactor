# Kombucha Reactor

Regulation and monitoring system for growing kombucha.
Developped for contribution to [Karp](https://thr34d5.org/2019/08/20/karp/)

# abstract

The function of this module is to monitor the growth of a kombucha scoby,
to be able to control the maturity of the solution (that will become a drink : mature in 4-7 days),
and the thickness of the scoby (that will be used as cellulose : mature in 15-20 days ).

The final version will offer following informations :
* thickness of the scoby
* sugar concentration of the solution (function of maturity of the solution)
* temperature

The frequency of the measures will be about 1 hour.


# Principe of operation

Most of the monitoring system is encapsulated in a test tube 
to avoid physical contact with the liquid.

![overview](overview.jpg "overview")

The main captor is an unidimensional CCD, used for the two mesures at different times.

## 1 Sugar concentration 

This value is monitored via [refractometry](https://fr.wikipedia.org/wiki/R%C3%A9fractom%C3%A9trie)

The laser diode emit a light, that is refracted by the liquid at differents angles function of the concentration of the solution.

The CCD detect the position of the incident ray.

See signals 1A and 1B for expected type of results out of the CCD.

## 2 scoby thickness

A laser diode is mounted on a rotating support, actionned by a servomotor, for scaning a range of angles.

The incident light coming to the CCD will be lighter when reflecting on the scoby

See signals 2A Aand 2B for expected results.

# ressources

## Kombutcha growth tips

https://thr34d5.org/2019/08/28/open-source-kombucha/


## TCD1304
linear CCD

https://hackaday.io/project/9829-linear-ccd-module

